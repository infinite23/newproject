//
//  Screen2Favorites.swift
//  NewProject
//

import UIKit

final class FavoritesViewController: UIViewController, UICollectionViewDelegate, CollectionViewCellProtocol, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var emptyView = EmptyFavorites()
    var link: FavoritesList!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView.reloadData()
        checkEmpty()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        link = FavoritesList.shared
        
        let collectionCellNib = UINib(nibName: "FavoritesCell", bundle: nil)
        collectionView.register(collectionCellNib, forCellWithReuseIdentifier: "FavoritesCell")
        
    }
    
    func checkEmpty() {
        if link.favorites.isEmpty {
            emptyView.show(in: view)
        } else {
            emptyView.hide()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return link.favorites.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoritesCell", for: indexPath) as? FavoritesCell {
            cell.idLabelFavs.text = String(link.favorites[indexPath.row])
            
            cell.layer.borderWidth = 2
            cell.layer.cornerRadius = 5
            
            cell.delegate = self
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width / 2.1, height: UIScreen.main.bounds.height / 5)
    }
    
    func favoriteAction(cell: UICollectionViewCell) {
        let indexPath = collectionView.indexPath(for: cell)
        let element = link.favorites[indexPath?.item ?? 0]
        
        link.remove(element: element)
        collectionView.reloadData()
        
        checkEmpty()

    }
}
