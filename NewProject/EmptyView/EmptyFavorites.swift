//
//  EmptyFavorites.swift
//  NewProject
//

import UIKit

final class EmptyFavorites: ViewToShow {
    
    override func getViewFromNib() -> UIView {
        let nib = UINib(nibName: "EmptyFavorites", bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView ?? UIView()
    }
}
