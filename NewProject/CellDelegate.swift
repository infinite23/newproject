//
//  CellDelegate.swift
//  NewProject
//

import UIKit

protocol TableViewCellProtocol: class {
     
    func favoriteAction(cell: UITableViewCell)
}

protocol CollectionViewCellProtocol: class {

    func favoriteAction(cell: UICollectionViewCell)
}
