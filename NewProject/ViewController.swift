//
//  FirstTableViewController.swift
//  AutosportInfo
//

import UIKit

final class ViewController: UIViewController {
    
    let updatingView = UpdatingView()
    var jsonInfo: DriversInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NetworkHelper.instance.getDriverStandings().then { result in
            self.jsonInfo = result
        }.catch { _ in
            
            let alert = UIAlertController(title: "Ошибка сети", message: "Вероятно, отсутствует подключение к интернету", preferredStyle: .alert)
            
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(action)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // Кнопка для вызова функций
    @IBAction func updater(_ sender: UIButton) {
        updatingView.show()
        //updatingView.show(in: view)
        
        // Добавил задержку, чтобы можно было потестить нажатие на кнопку во время работы функции
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            self.updatingView.hide()
        }
    }
}
