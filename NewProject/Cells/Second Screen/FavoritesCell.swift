//
//  Screen2CollectionViewCell.swift
//  NewProject
//

import UIKit

final class FavoritesCell: UICollectionViewCell {
    
    @IBOutlet weak var idLabelFavs: UILabel!
    @IBOutlet weak var removeFromFavorites: UIButton!
    
    weak var delegate: CollectionViewCellProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        removeFromFavorites.setImage(#imageLiteral(resourceName: "star"), for: .normal)
        removeFromFavorites.tintColor = .red
    }
    
    @IBAction func removeFromFavorites(_ sender: UIButton) {
        delegate?.favoriteAction(cell: self)
    }
}
