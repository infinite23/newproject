//
//  ImageViewer.swift
//  NewProject
//

import UIKit

final class ImageViewerViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var linkImages = [
        "https://images.unsplash.com/photo-1605915532179-0fd65403813e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80",
        "https://images.unsplash.com/photo-1605951119672-2ec512ff39cb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2100&q=80",
        "https://images.unsplash.com/photo-1606059979642-8390434af3d5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80",
        "https://images.unsplash.com/photo-1606089022569-b43201f36652?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2110&q=80"
    ]
    
    var currentPage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureShareButton()
        registerCells()
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss(sender:))))
    }
    
    private func configureShareButton() {
        navigationItem.title = "1 из \(linkImages.count)"
        let imageForSharedButton = #imageLiteral(resourceName: "shared")
        
        let shareButton = UIBarButtonItem(image: imageForSharedButton, style: .done, target: self, action: #selector(sharedAction))
        navigationItem.rightBarButtonItem = shareButton
    }
    
    private func registerCells() {
        let cellCollectionView = UINib(nibName: "ImageCell", bundle: nil)
        collectionView.register(cellCollectionView, forCellWithReuseIdentifier: "ImageCell")
    }
    
    func getImageForShare() {
        let index = linkImages[currentPage]
        
        guard let url = URL(string: index), let data = try? Data(contentsOf: url) else {
            return }
        
        let activityVC = UIActivityViewController(activityItems: [data], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = view
        
        present(activityVC, animated: true, completion: nil)
    }
    
    @objc func sharedAction() {
        getImageForShare()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        linkImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as? ImageCell {
            cell.imageView.setImage(from: URL(string: linkImages[indexPath.item]))
            return cell
        }
        return UICollectionViewCell()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = collectionView.contentOffset.x
        let w = collectionView.bounds.size.width
        
        currentPage = Int(x / w)
        navigationItem.title = "\(currentPage + 1) из \(linkImages.count)"
    }
    
    var viewTranslation = CGPoint(x: 0, y: 0) // Оставил тут, а не в начале файла, для читабельности
    
    @objc func handleDismiss(sender: UIPanGestureRecognizer) {
        switch sender.state {
            case .changed:
                viewTranslation = sender.translation(in: view)
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                    self.view.transform = CGAffineTransform(translationX: 0, y: self.viewTranslation.y)
                })
            case .ended:
                if viewTranslation.y < collectionView.frame.height / 5 {
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                        self.view.transform = .identity
                    })
                } else {
                    popToViewController()
                }
            default:
                break
        }
    }
        
    func popToViewController() {
        let transition = CATransition()
        transition.duration = 0.2
        
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        transition.type = CATransitionType.moveIn
        
        transition.subtype = CATransitionSubtype.fromBottom
        navigationController?.view.layer.add(transition, forKey: nil)
        navigationController?.popViewController(animated: false)
    }
}
