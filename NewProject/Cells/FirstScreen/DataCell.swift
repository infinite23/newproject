//
//  DataCell.swift
//  NewProject
//

import UIKit

final class DataCell: UITableViewCell {
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var addToFavoriteButtonLabel: UIButton!
    
    weak var delegate: TableViewCellProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        addToFavoriteButtonLabel.setImage(#imageLiteral(resourceName: "star"), for: .normal)
        accessoryView = addToFavoriteButtonLabel
    }
    
    @IBAction func action(_ sender: UIButton) {
        delegate?.favoriteAction(cell: self)
       }
   }
