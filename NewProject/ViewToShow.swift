//
//  ViewToShow.swift
//  NewProject
//

import UIKit

class ViewToShow: UIView {

    func getViewFromNib() -> UIView {
        let nib = UINib(nibName: "UpdatingView", bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView ?? UIView()
    }

    override init(frame: CGRect) {
       super.init(frame: frame)
       configureViews()
       
    }

    required init?(coder: NSCoder) {
       super.init(coder: coder)
       configureViews()
   }

    func configureViews() {
        let viewFromNib = getViewFromNib()
        viewFromNib.translatesAutoresizingMaskIntoConstraints = false
       
        addSubview(viewFromNib)
        NSLayoutConstraint.activate([
            viewFromNib.leadingAnchor.constraint(equalTo: leadingAnchor),
            viewFromNib.trailingAnchor.constraint(equalTo: trailingAnchor),
            viewFromNib.topAnchor.constraint(equalTo: topAnchor),
            viewFromNib.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
   }
    
    func show() {
        
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        translatesAutoresizingMaskIntoConstraints = false
        window.addSubview(self)
        
        alpha = 0.5
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: window.leadingAnchor),
            trailingAnchor.constraint(equalTo: window.trailingAnchor),
            topAnchor.constraint(equalTo: window.topAnchor),
            bottomAnchor.constraint(equalTo: window.bottomAnchor)
        ])
    }
    
    func show(in view: UIView) {
        var viewToShow: UIView
        
        viewToShow = view
        translatesAutoresizingMaskIntoConstraints = false
        
        viewToShow.addSubview(self)
        isUserInteractionEnabled = false
        
        NSLayoutConstraint.activate([
            centerYAnchor.constraint(equalTo: viewToShow.centerYAnchor),
            centerXAnchor.constraint(equalTo: viewToShow.centerXAnchor)
        ])
    }
    
    func hide() {
        isUserInteractionEnabled = true
        removeFromSuperview()
    }
}
