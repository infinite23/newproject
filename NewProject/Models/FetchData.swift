//
//  FetchData.swift
//  NewProject

struct FetchDataResult: Codable {
    
    let id: Int
    
    enum CodingKeys: String, CodingKey {
        case id
    }
}

typealias FetchData = [FetchDataResult]
