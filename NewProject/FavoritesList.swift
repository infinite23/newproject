//
//  FavoritesList.swift
//  NewProject
//

final class FavoritesList {
    
   static let shared = FavoritesList()
    
    var favorites: [String] = []
   
    func add(element: String ) {
            favorites.append(element)
    }
    
    func remove(element: String) { 
        if let index = favorites.firstIndex(of: element) {
            favorites.remove(at: index)
        }
    }
    
    func actionWithElement(element: String) {
        if !favorites.contains(element) {
            add(element: element)
        } else {
            remove(element: element)
        }
    }
}
