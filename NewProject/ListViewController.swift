//
//  Screen1ViewController.swift
//  NewProject
//

import UIKit

final class ListViewController: UIViewController, UITableViewDelegate, TableViewCellProtocol, UITableViewDataSource {
        
    @IBOutlet weak var tableView: UITableView!
    
    var jsonData: FetchData?
    var customCell: DataCell?
    
    var favs: FavoritesList!
    var updatingView = UpdatingView()

    var needToFetch = false
    var defaultPage = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.isHidden = true
        updatingView.show(in: view)
        
        favs = FavoritesList.shared
        let cellNib = UINib(nibName: "DataCell", bundle: nil)
        
        tableView.register(cellNib, forCellReuseIdentifier: "cell")
        beginLoading()
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.height * 4 {
            if !needToFetch {
                beginLoading()
            }
        }
    }
    
    func beginLoading() {
        needToFetch = true
        defaultPage += 1
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.needToFetch = false
            
            NetworkHelper.instance.getData(page: self.defaultPage).then { result in
                self.updatingView.hide()
                self.tableView.isHidden = false
                
                if self.defaultPage == 1 {
                    self.jsonData = result
                } else {
                    self.jsonData?.append(contentsOf: result)
                }
                self.tableView.reloadData()

            }.catch { _ in
                self.updatingView.hide()
                let alert = UIAlertController(title: "Ошибка сети", message: "Вероятно, отсутствует подключение к интернету", preferredStyle: .alert)
                
                let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(action)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? DataCell {
            let text = jsonData?[indexPath.row].id ?? 0
            cell.idLabel.text = "Row \(text)"
            cell.delegate = self
            cell.accessoryView?.tintColor = favs.favorites.contains(String(text)) ? UIColor.red : UIColor.gray
            return cell
        }
        return UITableViewCell()
    }
    
    // MARK: - TableViewDelegate
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        
        if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(cell.frame.height))
            self.tableView.tableFooterView = spinner
            
            if needToFetch == false {
                tableView.tableFooterView? = UIView()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let imageViewController = UIStoryboard(name: "ImageViewer", bundle: nil).instantiateViewController(withIdentifier: "ImageViewerViewController")
        navigationController?.pushViewController(imageViewController, animated: true)
    }
    
    func favoriteAction(cell: UITableViewCell) {
        let indexPath = tableView.indexPath(for: cell)
        let elementForFavorites = String(jsonData?[indexPath?.row ?? 0].id ?? 0)
        
        favs.actionWithElement(element: elementForFavorites)
        tableView.reloadData()
    }
}
