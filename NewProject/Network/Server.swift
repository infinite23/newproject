//
//  Server.swift
//  AutosportInfo
//

import Foundation

final class Server {
    
    // MARK: - Life Cycle
    
    private init() { }
    
    // MARK: - Static Functions
    
    static func getBaseURL() -> URL {
        return URL(string: "https://jsonplaceholder.typicode.com/")!
    }
    
}
