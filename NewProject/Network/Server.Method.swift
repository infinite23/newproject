//
//  Server.Method.swift
//  AutosportInfo
//

import Moya

extension Server {
    
    enum Method: TargetType {
        
        case getData(page: Int)
        
        case getDriverStandings
        case getTeamStandings
        
        case getCircuitsInfo
        case getRacesResults
        
        // MARK: - Configurations
        
        var baseURL: URL {
            return Server.getBaseURL()
        }
        
        var path: String {
            var target: String
            
            switch self {
                case .getData:
                    target = "posts"
                
                case .getDriverStandings:
                    target = "DriverStandings.json"
                    
                case .getTeamStandings:
                   target = "ConstructorStandings.json"
                    
                case .getCircuitsInfo:
                    target = "Circuits/.json"
                  
                case .getRacesResults:
                    target = "results.json?limit=1000"
            }
            return "\(target)"
        }
        
        var task: Task {
            
            switch self {
                case .getData(let page):
                    return Task.requestParameters(parameters: ["_limit": 20, "_page": page], encoding: URLEncoding.queryString)
                default:
                    return Task.requestPlain
            }
        }
        
        var method: Moya.Method {
            switch self {
                case .getData:
                    return .get
                
                case .getDriverStandings:
                    return .get
                    
                case .getTeamStandings:
                    return .get
                    
                case .getCircuitsInfo:
                    return .get
                    
                case .getRacesResults:
                    return .get
            }
        }
    
        var headers: [String: String]? { .none }
    
        var sampleData: Data {
            return Data()
        }
    }
}
